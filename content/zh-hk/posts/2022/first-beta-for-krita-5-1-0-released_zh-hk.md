---
title: "發佈 Krita 5.1.0 首個測試版本 (Beta 1)"
date: "2022-06-23"
categories: 
  - "news_zh-hk"
  - "development-builds_zh-hk"
---

今日我哋發佈咗 Krita 5.1.0 嘅第一個測試版本。Krita 5.1.0 帶嚟咗好多新功能呀！想知道完整嘅新功能清單嘅話，可以睇下仲未寫完嘅[英文版發佈通告](https://krita.org/en/krita-5-1-release-notes/)喎！

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 係自由、免費同開源嘅專案。請考慮[加入 Krita 發展基金](https://fund.krita.org/)、[損款](https://krita.org/en/support-us/donations/)，或者[購買教學影片](https://krita.org/en/shop/)支持我哋啦！有你哋嘅熱心支持，我哋先可以俾核心開發者全職為 Krita 工作。

## 重點新功能

- 更多操作可以支援同時處理多過一個已選取圖層。
- 我哋改善咗對 WebP 檔案、帶有 Photoshop 圖層嘅 TIFF 檔案，以及 Photoshop 檔案嘅支援。同時，我哋亦都新增咗支援 JPEG XL 檔案格式。（但係請留意下面嘅已知問題。）
- Krita 依家用咗 XSIMD 程式庫嚟取代 Vc 做 SIMD（單指令多資料流）加速，以進一步提升筆刷效能。依樣嘢對於喺 Android 裝置上運行嘅效能影響尤其大，因為依個係首個支援 ARM 架構嘅 SIMD 加速指令集嘅 Krita 版本。
- 填充工具已擴展到去支援「多重填充」（連續模式），另外亦都新加咗一個「閉合區域填充工具」。
- 我哋為 Windows 版本更新咗 ANGLE 程式庫，以改善圖形加速嘅兼容性同埋效能。
- 你依家可以喺「畫布輸入設定」入面設定觸控手勢 (touchscreen) 操作，例如用兩指點選嚟復原。

當然，仲有大量各項修正、效能提升、介面改進，連帶動畫功能都有啲改善。（不過動畫音效系統嘅大修趕唔切喺依個版本推出，要留俾 5.2 版本喇。）

## 已知問題

- 依個版本可以自訂觸控手勢輸入，不過如果你喺舊版有用到觸控輸入嘅話，你需要喺「畫布輸入設定」入面重新加入同設定觸控手勢，咁先至可以繼續用觸控輸入。
- 雖然依個版本已經加入咗 JPEG XL 動畫嘅支援，但係喺匯入同埋匯出 JPEG XL 動畫時已知道會產生唔啱嘅輸出。我哋已經喺度修正緊依個問題。

[![5.1.0-beta1 介面截圖](images/5.1.0-beta1-1024x562.png)](https://krita.org/wp-content/uploads/2022/06/5.1.0-beta1.png)

## 下載

### Windows

如果你使用免安裝版：請注意，免安裝版仍然會同安裝版本共用設定檔同埋資源。如果想用免安裝版測試並回報 crash 嘅問題，請同時下載偵錯符號 (debug symbols)。

注意：我哋依家唔再提供為 32 位元 Windows 建置嘅版本。

- 64 位元安裝程式：[krita-x64-5.1.0-beta1-setup.exe](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x64-5.1.0-beta1-setup.exe)
- 64 位元免安裝版：[krita-x64-5.1.0-beta1.zip](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x64-5.1.0-beta1.zip)
- [偵錯符號 Debug symbols（請解壓到 Krita 程式資料夾入面）](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x64-5.1.0-beta1-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.0-beta1-x86\_64.appimage](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1-x86_64.appimage)

Linux 版本依家唔使再另外下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果你用緊 macOS Sierra 或者 High Sierra，請睇下[依段影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解點樣執行開發者簽署嘅程式。

- macOS 軟件包：[krita-5.1.0-beta1.dmg](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1.dmg)

### Android

我哋提供嘅 ChomeOS 同 Android 版本仲係**測試版本**。依個版本或可能含有大量嘅 bug，而且仲有部份功能未能正常運作。由於使用者介面仲未改進好，軟件或者須要配合實體鍵盤先可以用到全部功能。Krita 唔啱俾 Android 智能電話用，只係啱平板電腦用，因為使用者介面嘅設計並未為細小嘅螢幕做最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x86_64-5.1.0-beta1-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-x86-5.1.0-beta1-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-arm64-v8a-5.1.0-beta1-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-armeabi-v7a-5.1.0-beta1-release-signed.apk)

### 原始碼

- [krita-5.1.0-beta1.tar.gz](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1.tar.gz)
- [krita-5.1.0-beta1.tar.xz](https://download.kde.org/unstable/krita/5.1.0-beta1/krita-5.1.0-beta1.tar.xz)
