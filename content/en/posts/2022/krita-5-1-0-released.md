---
title: "Krita 5.1.0 Released!"
date: "2022-08-18"
categories: 
  - "news"
  - "officialrelease"
---

Today we're releasing Krita 5.1.0, a major new feature release!

## Highlights

- Even more operations can handle multiple selected layers.
- We've improved support for the WebP, Photoshop layered TIFF and Photoshop files, and there's new support for the JPEG-XL file format.
- We're now using XSIMD instead of VC, improving painting performance, especially on Android where vectorization now is used for the first time.
- The fill tools have been extended with continuous fill and a new Enclose and fill tool.
- For Windows, we're using a newer version of Angle to improve compatibility with video drivers and improve performance.
- You can now configure touch controls in canvas input settings, like "Tap to Undo".

And there are, of course, a lot of bug fixes, performance improvements and all kinds of little user interface polishing. For the full list, check out the full [release notes](https://krita.org/en/krita-5-1-release-notes/)!


{{< youtube TnvCjziCUGI >}}
 



{{< support-krita-callout >}}



[![Screenshot of 5.1.0-beta1](images/posts/2022/5.1.0-beta1-1024x562.webp)](https://krita.org/wp-content/uploads/2022/06/5.1.0-beta1.png)

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.1.0-setup.exe](https://download.kde.org/stable/krita/5.1.0/krita-x64-5.1.0-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.1.0.zip](https://download.kde.org/stable/krita/5.1.0/krita-x64-5.1.0.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.1.0/krita-x64-5.1.0-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.0-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0-x86_64.appimage)

The separate gmic-qt appimage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.1.0.dmg](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface needs a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.0/krita-x86_64-5.1.0-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.1.0/krita-x86-5.1.0-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.0/krita-arm64-v8a-5.1.0-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.1.0/krita-armeabi-v7a-5.1.0-release-signed.apk)

### Source code

- [krita-5.1.0.tar.gz](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0.tar.gz)
- [krita-5.1.0.tar.xz](https://download.kde.org/stable/krita/5.1.0/krita-5.1.0.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/stable/krita/5.1.0/](https://download.kde.org/stable/krita/5.1.0) and click on Details to get the hashes.

### Key

The Linux appimage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/4DA79EDA231C852B). The signatures are [here](https://download.kde.org/stable/krita/5.1.0/) (filenames ending in .sig).
