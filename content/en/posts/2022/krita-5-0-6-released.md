---
title: "Krita 5.0.6 Released"
date: "2022-04-27"
categories: 
  - "news"
  - "officialrelease"
---

Today we release Krita 5.0.6. This is a bug fix release with two crash fixes:

- A crash when working with vector layers or vector selections and using undo a lot: [BUG:447985](https://bugs.kde.org/show_bug.cgi?id=447985)
- A crash deleting a vector layer with an animated transparency mask: [BUG:452396](https://bugs.kde.org/show_bug.cgi?id=452396)

{{< support-krita-callout >}}

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.0.6-setup.exe](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.0.6.zip](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/stable/krita/5.0.6/krita-x64-5.0.6-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.0.6-x86\_64.appimage](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6-x86_64.appimage)

The separate gmic-qt appimage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.0.6.dmg](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface needs a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-x86_64-5.0.6-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-x86-5.0.6-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-arm64-v8a-5.0.6-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/stable/krita/5.0.6/krita-armeabi-v7a-5.0.6-release-signed.apk)

### Source code

- [krita-5.0.6.tar.gz](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.tar.gz)
- [krita-5.0.6.tar.xz](https://download.kde.org/stable/krita/5.0.6/krita-5.0.6.tar.xz)

### md5sum

For all downloads:

- [https://download.kde.org/stable/krita/5.0.6/](https://download.kde.org/stable/krita/5.0.6) and click on Details to get the hashes.

### Key

The Linux appimage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/4DA79EDA231C852B). The signatures are [here](https://download.kde.org/stable/krita/5.0.6/) (filenames ending in .sig).
